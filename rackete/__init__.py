# -*- coding: utf-8 -*-

"""Console script for rackete."""
import sys
import click
import os
import os.path

from ruamel.yaml import YAML
from jinja2 import Environment, PackageLoader, select_autoescape

jinja_env = Environment(
    loader=PackageLoader('rackete', 'templates'),
    autoescape=select_autoescape(['html', 'xml'])
)


yaml = YAML(typ='safe')

here = os.path.abspath(os.path.split(__file__)[0])

data_folder = os.path.abspath(os.path.join(here, '../data'))

class Block:
    def __init__(self, path):
        self.path = path
        with open(path) as f:
            data = yaml.load(f)
        self.name = data['name']
        if 'key' in data:
            self.key = data['key']
        else:
            self.key = self.name.lower()
        self.items = [Item(i, self) for i in data['items']]

class Item:
    def __init__(self, data, block):
        self.block = block
        for key, val in data.items():
            setattr(self, key, val)
        if not 'key' in data:
            self.key = self.name.lower()

        statement_path = os.path.join(data_folder, self.block.key, self.key + '.html')
        if os.path.isfile(statement_path):
            self.statement_path = statement_path
        else:
            self.statement_path = None

    @property
    def has_statement(self):
        return bool(self.statement_path)


    @property
    def statement(self):
        with open(self.statement_path) as f:
            return f.read()


@click.command()
def main(args=None):
    """Console script for rackete."""
    click.echo(here)

    blocks = {}
    for item in os.listdir(data_folder):
        if item.endswith('.yaml'):
            b = Block(os.path.join(data_folder, item))
            blocks[b.key] = b


    index = jinja_env.get_template('index.html')

    target = os.path.abspath(os.path.join(here,'../public'))
    os.makedirs(target, exist_ok=True)

    with open(os.path.join(target,'index.html'),'w') as out:
        out.write(index.render(blocks=blocks))
    return 0


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
