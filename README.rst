=======
rackete
=======


.. image:: https://img.shields.io/pypi/v/rackete.svg
        :target: https://pypi.python.org/pypi/rackete

.. image:: https://img.shields.io/travis/domma/rackete.svg
        :target: https://travis-ci.org/domma/rackete

.. image:: https://readthedocs.org/projects/rackete/badge/?version=latest
        :target: https://rackete.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Python Boilerplate contains all the boilerplate you need to create a Python package.


* Free software: GNU General Public License v3
* Documentation: https://rackete.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
